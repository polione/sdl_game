//
//  player.c
//  sdl_game
//
//  Created by Giorgio Pogliani on 20/04/16.
//  Copyright © 2016 Giorgio Pogliani. All rights reserved.
//

#include "player.h"


Player* InitPlayer(){
    return calloc(1, sizeof(Player));
}

void moveRight(Player *player){
    cpVect velocity = cpBodyGetVelocity(player->physicBody);
    cpBodyApplyImpulseAtWorldPoint(player->physicBody, cpv(0.0025, 0), cpBodyGetVelocity(player->physicBody));
    
    if(velocity.x >= 0.000400){
        cpBodySetVelocity(player->physicBody, cpv(0.000400, velocity.y));
    }
}

void moveLeft(Player *player){
    cpVect velocity = cpBodyGetVelocity(player->physicBody);
    cpBodyApplyImpulseAtWorldPoint(player->physicBody, cpv(-0.0025, 0), cpBodyGetVelocity(player->physicBody));
    
    if(velocity.x <= -0.000400){
        cpBodySetVelocity(player->physicBody, cpv(-0.000400, velocity.y));
    }
}

void jump(Player *player){
    if(player->jumping){
        player->jumping = false;
        printf("Jumping!, %f\n",cpBodyGetVelocity(player->physicBody).y );
        cpFloat x =  cpBodyGetVelocity(player->physicBody).y;
        if (x < 0.00001 && x > -0.00001){
            cpBodySetForce(player->physicBody, cpv(0,-0.000000999));
        }
    }
}

void ResetPlayerPosition(Player* player){
    cpBodySetPosition(player->physicBody, cpv(50,0));
    cpBodySetVelocity(player->physicBody, cpv(0,0));
}

bool CheckGameOver(Player *player){
    cpVect pos = cpBodyGetPosition(player->physicBody);
    if(pos.y > WINDOW_SIZE_HEIGHT){
        ResetPlayerPosition(player);
        return true;
    }
    return false;
}
