//
//  global.c
//  sdl_game
//
//  Created by Giorgio Pogliani on 20/04/16.
//  Copyright © 2016 Giorgio Pogliani. All rights reserved.
//

#include "global.h"

Win getSize(){
    Win win;
    SDL_DisplayMode DM;
    SDL_GetCurrentDisplayMode(0, &DM);
    win.width = DM.w;
    win.height = DM.h;
    printf("%d, %d\n", win.height,win.width);
    return win;
}