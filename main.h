//
//  main.h
//  sdl_game
//
//  Created by Giorgio Pogliani on 31/03/16.
//  Copyright © 2016 Giorgio Pogliani. All rights reserved.
//



#include "global.h"
#include "player.h"

#ifndef main_h
#define main_h


SDL_Rect CreateRect();

static Player *player;

#define WSIZE 60
#define HSIZE 60

float targetFrameRate = 16.666666667;
int quitting = false;


#endif /* main_h */
