//
//  main.c
//  test
//
//  Created by Giorgio Pogliani on 28/03/16.
//  Copyright © 2016 Giorgio Pogliani. All rights reserved.
//





#include "main.h"


SDL_Rect CreateRect(int X, int Y, int Z, int W) {SDL_Rect k;k.x=X;k.y=Y;k.w=Z;k.h=W;return k;}



void InitLevel(){
    for (int i=0; i<50; i=i+1) {
        level[i] = CreateRect((i * 2*WSIZE) + i*(2*WSIZE), GROUND, 2*WSIZE, HSIZE);
    }
    ground.shapes = SDL_malloc( 50 * sizeof( cpShape** ) );
    for (int i=0; i<50; i++) {
        ground.shapes[i] = cpSegmentShapeNew(cpSpaceGetStaticBody(space), cpv(level[i].x,level[i].y), cpv(level[i].x + (2*WSIZE),level[i].y), 0);
        cpShapeSetFriction(ground.shapes[i], 0.5);
        cpShapeSetElasticity(ground.shapes[i], 0);
        cpSpaceAddShape(space,ground.shapes[i]);
    }
}


void EventHandling(){


    const Uint8*  keystates = SDL_GetKeyboardState(NULL);
    
    if(keystates[SDL_SCANCODE_UP])
    {
        player->jumping = true;
    }
    if(keystates[SDL_SCANCODE_LEFT]){
        moveLeft(player);
    }
    if(keystates[SDL_SCANCODE_RIGHT]){
        moveRight(player);
    }

    
    SDL_Point touchLocation = { getSize().width / 2, getSize().height / 2 };
    SDL_Event event;
    
    static bool left, right, up;
    
    while(SDL_PollEvent(&event)){
        if(event.type == SDL_QUIT) {
            quitting = true;
        }else if( event.type == SDL_FINGERDOWN )
        {
            touchLocation.x = event.tfinger.x * getSize().width;
            touchLocation.y = event.tfinger.y * getSize().height;
            if(touchLocation.x < getSize().width/2 && touchLocation.y < getSize().width/2){
                left = true;
            }
            if(touchLocation.x > getSize().width/2 && touchLocation.y < getSize().width/2){
                right = true;
            }
            if (touchLocation.y > getSize().width/2) {
                up = true;
            }
            
        }else if( event.type == SDL_FINGERUP )
        {
            touchLocation.x = event.tfinger.x * getSize().width;
            touchLocation.y = event.tfinger.y * getSize().height;
            if(touchLocation.x < getSize().width/2 && touchLocation.y < getSize().width/2){
                left = false;
            }
            if(touchLocation.x > getSize().width/2 && touchLocation.y < getSize().width/2){
                right = false;
            }
            if (touchLocation.y > getSize().width/2) {
                up = false;
            }
            
        }else if( event.type == SDL_WINDOWEVENT )
        {
            //Resize the screen
            //SDL_SetWindowSize(gWindow, getSize().width, getSize().height);
        }
    }
    
    if(left){
        moveLeft(player);
    }
    if(right){
        moveRight(player);
    }
    if(up){
        player->jumping = true;
    }
    
}



int SDLCALL watch(void *userdata, SDL_Event* event) {
	if (event->type == SDL_APP_WILLENTERBACKGROUND) {
		quitting = true;
	}
	return 1;
}





void Close(){
	SDL_DelEventWatch(watch, NULL);
	SDL_DestroyWindow(gWindow);
	SDL_DestroyRenderer(gRenderer);
	SDL_Quit();
}


void SetupRenderer()
{
    
    SDL_RenderSetLogicalSize( gRenderer, WINDOW_SIZE_WIDTH, WINDOW_SIZE_HEIGHT);
    SDL_SetRenderDrawColor(gRenderer, 45, 140, 250, 255);
}

bool CreateRenderer()
{
    gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if ( gRenderer == NULL ){
        printf("Failed to create renderer : %s",SDL_GetError());
        return false;
    }
    return true;
}

bool CreateWindow()
{
    
    getSize();
    gWindow = SDL_CreateWindow("Window",
                               SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED,
                               WINDOW_SIZE_WIDTH,
                               WINDOW_SIZE_HEIGHT,
                               SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI
                               );
    if ( gWindow == NULL )
    {
        printf( "Failed to create window : %s",SDL_GetError());
        return false;
    }
    return true;
}

void CreateTextTextures()
{
    ground.texture = IMG_LoadTexture(gRenderer, "ground.png");
    if(ground.texture == NULL){
        printf( "Failed to create window : %s",SDL_GetError());
    }
    player->texture = IMG_LoadTexture(gRenderer, "player.png");
    if(player->texture == NULL){
        printf( "Failed to create window : %s",SDL_GetError());
    }
}

bool CreatePhysics(){

    cpVect gravity = cpv(0, 0.000000007 );
    space = cpSpaceNew();
    cpSpaceSetGravity(space, gravity);
    if(space == NULL){
        return false;
    }
    
    int playerHeight = 60, playerWidth = 60;
    
    player->physicBody = cpSpaceAddBody(space, cpBodyNew(10, INFINITY));
    player->shapeBody = cpBoxShapeNew(player->physicBody, playerHeight, playerWidth, 1);
    player->rect.w = playerWidth;
    player->rect.h = playerHeight;
    
    cpShapeSetFriction(player->shapeBody, 1);
    cpShapeSetElasticity(player->shapeBody, 0);
    cpBodySetPosition(player->physicBody, cpv(0, GROUND - 2*playerWidth));
    cpBodySetVelocity(player->physicBody, cpv(0,0));
    cpSpaceAddShape(space,player->shapeBody);
    
    return true;
}

bool InitSDL(){
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf( "Failed to create window : %s",SDL_GetError());
        return false;
    }
    return true;
}

bool InitEverything()
{
    if ( !InitSDL() )
        return false;
    
    if ( !CreateWindow() )
        return false;

    if ( !CreateRenderer() )
        return false;

    SDL_AddEventWatch(watch, NULL); //quit
    
    player = InitPlayer();
    if( player ==  NULL)
        return false;
    
    if ( !CreatePhysics() )
        return false;
    
    SetupRenderer();
    CreateTextTextures();
    InitLevel();
    
    
    return true;
}


void Render()
{
    SDL_RenderClear( gRenderer );
    for(int x=0; x<=50; x++) {
        SDL_RenderCopy( gRenderer, ground.texture, NULL, &level[x] );
    }
    SDL_RenderCopy( gRenderer, player->texture, NULL, &player->rect );
    SDL_RenderPresent( gRenderer);
}


void SyncTextures(){
    cpVect pos = cpBodyGetPosition(player->physicBody);
    player->rect.y = floor(pos.y) - HSIZE/2;
    player->rect.x = floor(pos.x) - HSIZE/2;
    
    for (int i=0; i<50; i++) {
        cpSpaceRemoveShape(space, ground.shapes[i]);
    }
    for (int i=0; i<50; i=i+1) {
        level[i] = CreateRect(camera.x + (i * 2*WSIZE) + i*(2*WSIZE), GROUND, 2*WSIZE, HSIZE);
    }
    for (int i=0; i<50; i++) {
        ground.shapes[i] = cpSegmentShapeNew(cpSpaceGetStaticBody(space), cpv(level[i].x,level[i].y), cpv(level[i].x + (2*WSIZE),level[i].y), 0);
        cpShapeSetFriction(ground.shapes[i], 0.5);
        cpShapeSetElasticity(ground.shapes[i], 0);
        cpSpaceAddShape(space,ground.shapes[i]);
    }
}



void MoveCamera(){
    cpVect pos = cpBodyGetPosition(player->physicBody);
    
    printf( "%d\n",getSize().width/2);
    
    if(camera.x != pos.x && pos.x > getSize().width/2 && cpBodyGetVelocity(player->physicBody).x > 0){
        int f =  -10;
        camera.x += f;
        cpBodySetPosition(player->physicBody, cpv(cpBodyGetPosition(player->physicBody).x + f, cpBodyGetPosition(player->physicBody).y));
    }else if(camera.x != pos.x && pos.x < getSize().width/2 && cpBodyGetVelocity(player->physicBody).x < 0){
        int f =  10;
        camera.x += f;
        cpBodySetPosition(player->physicBody, cpv(cpBodyGetPosition(player->physicBody).x + f, cpBodyGetPosition(player->physicBody).y));
    }
}


void Rungame(){
    
    EventHandling();
    
    Render();
    
    SyncTextures();

    MoveCamera();
    
    if(CheckGameOver(player)){
        camera.x = 0;
        SyncTextures();
        
    }
    jump(player);
    
    cpSpaceStep(space, targetFrameRate * 1000);
    SDL_Delay(targetFrameRate);
}


int main(int argc, char *argv[]) {

    
    if ( !InitEverything() )
        return -1;
    
    while(!quitting)
        Rungame();
    
    Close();
    return 0;

} //main