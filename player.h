//
//  player.h
//  sdl_game
//
//  Created by Giorgio Pogliani on 20/04/16.
//  Copyright © 2016 Giorgio Pogliani. All rights reserved.
//

#ifndef player_h
#define player_h

#include "global.h"

struct Player{
    cpShape *shapeBody;
    cpBody *physicBody;
    SDL_Texture *texture;
    SDL_Rect rect;
    bool jumping;
};
typedef struct Player Player;


Player* InitPlayer();

void moveRight(Player *player);
void moveLeft(Player *player);
void jump(Player *player);
bool CheckGameOver(Player *player);

#endif /* player_h */
