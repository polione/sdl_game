//
//  global.h
//  sdl_game
//
//  Created by Giorgio Pogliani on 20/04/16.
//  Copyright © 2016 Giorgio Pogliani. All rights reserved.
//

#ifndef global_h
#define global_h



#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>


#include <chipmunk.h>
#include <stdbool.h>
#include <stdio.h>

struct World{
    cpShape **shapes;
    SDL_Texture *texture;
    SDL_Rect rect;
};
static struct World ground;

typedef struct Win{int width,height;} Win;

static cpSpace *space;

static SDL_Renderer *gRenderer;
static SDL_Window *gWindow;
static SDL_Rect level[50];
static SDL_Rect camera;

Win getSize();

//#if OSX
#define WINDOW_SIZE_WIDTH getSize().width
#define WINDOW_SIZE_HEIGHT getSize().height
//#else
//#define WINDOW_SIZE_WIDTH  1334
//#define WINDOW_SIZE_HEIGHT 750
//#endif
#define GROUND  (getSize().height - 60)

#endif /* global_h */
