# SDL Game Test

## Requirements for linux (Ubuntu):
- libsdl2-dev
- libsdl2-image-dev
- libsdl2-ttf-dev
- libbsd-dev (arc4random, i just like it)
- chipmunk-dev (ver 7.0)

`sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libbsd-dev chipmunk-dev`

## Requirements for OSX (homebrew):
- sdl2
- sdl2_image
- sdl2_ttf
- chipmunk

`brew install sdl2 sdl2_image sdl2_ttf chipmunk`

## Build:

`make`
