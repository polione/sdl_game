OS = $(shell uname)

ifeq "$(OS)" "Darwin"
	CHIPMUNK = /usr/local/opt/chipmunk/include/chipmunk
	LIBCHIPMUNK = -L/usr/local/opt/chipmunk/lib
else
	CHIPMUNK = /usr/local/include/chipmunk
	LIBCHIPMUNK = -L/usr/local/lib
	LIBBSD = -lbsd
endif

SDL2 = $(shell pkg-config --cflags --libs SDL2_image sdl2 SDL2_ttf)

all:
	gcc main.c player.c global.c $(SDL2) -I$(CHIPMUNK) $(LIBCHIPMUNK) -lm -o chip_main $(LIBBSD) -lchipmunk